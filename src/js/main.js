'use stricts';

import * as noUiSlider from 'nouislider';

// Рэйндж сладер //

var sliderPrice = document.getElementById('range-prace');
var sliderSquare = document.getElementById('range-square');
let amount = 5,
    dataLength = null,
    getMore = 20,
    section = document.querySelector('.section');

var marginMinPrice = document.getElementById('price_from'),
    marginMaxPrice = document.getElementById('price_yes'),
    inputsPrice = [marginMinPrice, marginMaxPrice],
    filterReset = document.querySelector('.filter__all_reset_btn');

if (sliderPrice) {
    noUiSlider.create(sliderPrice, {
        start: [1000000, 30000000],
        keyboardSupport: true,
        connect: true,
        range: {
            min: 1000000,
            max: 30000000,
        },
    });

    sliderPrice.noUiSlider.on('update', function (values, handle) {
        if (handle) {
            marginMaxPrice.value = Math.trunc(values[handle]);
            marginMaxPrice.defaultValue = Math.trunc(values[handle]);
        } else {
            marginMinPrice.value = Math.trunc(values[handle]);
            marginMinPrice.defaultValue = Math.trunc(values[handle]);
        }
    });

    inputsPrice.forEach(function (input, handle) {
        input.addEventListener('change', function () {
            sliderPrice.noUiSlider.setHandle(handle, this.value);
        });

        input.addEventListener('keydown', function (e) {
            var values = sliderPrice.noUiSlider.get();
            var value = Number(values[handle]);

            // [[handle0_down, handle0_up], [handle1_down, handle1_up]]
            var steps = sliderPrice.noUiSlider.steps();

            // [down, up]
            var step = steps[handle];

            var position;

            // 13 is enter,
            // 38 is key up,
            // 40 is key down.
            switch (e.which) {
                case 13:
                    sliderPrice.noUiSlider.setHandle(handle, this.value);
                    break;
                case 38:
                    // Get step to go increase slider value (up)
                    position = step[1];
                    // false = no step is set
                    if (position === false) {
                        position = 1;
                    }
                    // null = edge of slider
                    if (position !== null) {
                        sliderPrice.noUiSlider.setHandle(handle, value + position);
                    }
                    break;
                case 40:
                    position = step[0];

                    if (position === false) {
                        position = 1;
                    }

                    if (position !== null) {
                        sliderPrice.noUiSlider.setHandle(handle, value - position);
                    }
                    break;
            }
        });
    });
}

if (sliderSquare) {
    noUiSlider.create(sliderSquare, {
        start: [10, 200],
        keyboardSupport: true,
        connect: true,
        range: {
            min: 10,
            max: 200,
        },
    });
    var marginMinSquare = document.getElementById('square_from'),
        marginMaxSquare = document.getElementById('square_yes'),
        inputsSquare = [marginMinSquare, marginMaxSquare];

    sliderSquare.noUiSlider.on('update', function (values, handle) {
        if (handle) {
            marginMaxSquare.value = Math.trunc(values[handle]);
            marginMaxSquare.defaultValue = Math.trunc(values[handle]);
        } else {
            marginMinSquare.value = Math.trunc(values[handle]);
            marginMinSquare.defaultValue = Math.trunc(values[handle]);
        }
    });
    inputsSquare.forEach(function (input, handle) {
        input.addEventListener('change', function () {
            sliderSquare.noUiSlider.setHandle(handle, this.value);
        });

        input.addEventListener('keydown', function (e) {
            var values = sliderSquare.noUiSlider.get();
            var value = Number(values[handle]);

            // [[handle0_down, handle0_up], [handle1_down, handle1_up]]
            var steps = sliderSquare.noUiSlider.steps();

            // [down, up]
            var step = steps[handle];

            var position;

            // 13 is enter,
            // 38 is key up,
            // 40 is key down.
            switch (e.which) {
                case 13:
                    sliderSquare.noUiSlider.setHandle(handle, this.value);
                    break;
                case 38:
                    // Get step to go increase slider value (up)
                    position = step[1];
                    // false = no step is set
                    if (position === false) {
                        position = 1;
                    }
                    // null = edge of slider
                    if (position !== null) {
                        sliderSquare.noUiSlider.setHandle(handle, value + position);
                    }
                    break;
                case 40:
                    position = step[0];

                    if (position === false) {
                        position = 1;
                    }

                    if (position !== null) {
                        sliderSquare.noUiSlider.setHandle(handle, value - position);
                    }
                    break;
            }
        });
    });
}

// Рэйндж сладер ---------//

const filter = document.querySelector('.filter'),
    apartmentsList = document.querySelector('.apartments__list'),
    filterRoomInput = document.querySelectorAll('.filter__room_input'),
    sectionLoadMore = document.querySelector('.section__load_more'),
    priceSortUp = document.querySelector('#sort__price_up'),
    sortPriceDown = document.querySelector('#sort__price_down'),
    sortLevelDown = document.querySelector('#sort__level_down'),
    sortLevelUp = document.querySelector('#sort__level_up'),
    sortSquareUp = document.querySelector('#sort__square_up'),
    sortSquareDown = document.querySelector('#sort__square_down'),
    apartmentsPrice = document.querySelector('.apartments__price'),
    apartmentsFloor = document.querySelector('.apartments__floor'),
    apartmentsSquares = document.querySelector('.apartments__squares'),
    sortReset = document.querySelectorAll('.sort__reset'),
    apartmentsArrowBtnButton = document.querySelectorAll('.apartments__arrow_btn_button');

// Фильтр контента //

const load = () => {
    fetch('./resources/data.json')
        .then((response) => {
            return response.json();
        })

        .then((data) => {
            filterItems();
            filter.addEventListener('input', filterItems);
            filter.addEventListener('click', filterItems);

            function filterItems() {
                const level = [...filter.querySelectorAll('.filter__room input:checked')].map(
                        (el) => el.value,
                    ),
                    priceMin = document.querySelector('#price_from').value,
                    priceMax = document.querySelector('#price_yes').value,
                    squareMin = document.querySelector('#square_from').value,
                    squareMax = document.querySelector('#square_yes').value;

                // outputItems(data.sort((el) => !priceMin || priceMin >= +el.squared));

                outputItems(
                    data.filter(
                        (el) =>
                            (!level.length || level.includes(el.rooms)) &&
                            (!priceMin || priceMin <= el.price) &&
                            (!priceMax || priceMax >= el.price) &&
                            (!squareMin || squareMin <= +el.squared) &&
                            (!squareMax || squareMax >= +el.squared),
                    ),
                );
            }

            function outputItems(data) {
                dataLength = data.length;
                apartmentsList.innerHTML = '';
                for (let i = 0; i < dataLength; i++) {
                    if (i < amount) {
                        let item = data[i];
                        apartmentsList.innerHTML += `
                        <div class="apartments__items">
                        <img class="apartments__img_plan" src="${item.img}" alt="Планировка" />
                        <a class="apartments__items_title" href="#">${item.title}</a>
                        <div class="apartments__items_squares">${item.squared.replace(
                            '.',
                            ',',
                        )}</div>
                        <div class="apartments__items_level">${
                            item.floor
                        }&nbsp<span>из 17</span></div>
                        <div class="apartments__items_price">${item.price.toLocaleString(
                            'ru-RU',
                        )}</div>
                    </div>`;
                    }
                }
            }

            sectionLoadMore.addEventListener('click', () => {
                amount = amount + getMore;
                outputItems(data);
                if (amount >= dataLength) {
                    sectionLoadMore.style.display = 'none';
                } else {
                    sectionLoadMore.style.display = 'block';
                }
            });
        });
};

load();

// Фильтр контента -------//

// Сортировка списка //

priceSortUp.addEventListener('click', function (elem) {
    apartmentsArrowBtnButton.forEach((el) => {
        el.classList.remove('active__sort');
    });
    elem.currentTarget.classList.toggle('active__sort');
    apartmentsPrice.style.color = '#3EB57C';
    apartmentsFloor.style.color = '#0b1739';
    apartmentsSquares.style.color = '#0b1739';
    var items = document.querySelectorAll('.apartments__items');
    var SortElements = new Object();
    items.forEach(function (item, indx) {
        var itemValue = parseInt(
            item
                .querySelector('.apartments__items_price')
                .textContent.replace('руб', '')
                .replace(/\s+/g, ''),
        );
        SortElements[itemValue] = { element: item, index: indx };
    });
    var keys = Object.keys(SortElements);
    function compareNumeric(a, b) {
        a = parseInt(a);
        b = parseInt(b);
        if (a > b) return -1;
        if (a < b) return 1;
    }
    keys.sort(compareNumeric);
    keys.map(function (key) {
        apartmentsList.insertAdjacentElement('beforeend', SortElements[key]['element']);
    });
});

sortPriceDown.addEventListener('click', function (elem) {
    apartmentsArrowBtnButton.forEach((el) => {
        el.classList.remove('active__sort');
    });
    elem.currentTarget.classList.toggle('active__sort');
    apartmentsPrice.style.color = '#3EB57C';
    apartmentsFloor.style.color = '#0b1739';
    apartmentsSquares.style.color = '#0b1739';
    var items = document.querySelectorAll('.apartments__items');
    var SortElements = new Object();
    items.forEach(function (item, indx) {
        var itemValue = parseInt(
            item
                .querySelector('.apartments__items_price')
                .textContent.replace('руб', '')
                .replace(/\s+/g, ''),
        );
        SortElements[itemValue] = { element: item, index: indx };
    });
    var keys = Object.keys(SortElements);
    function compareNumeric(a, b) {
        a = parseInt(a);
        b = parseInt(b);
        if (a < b) return -1;
        if (a > b) return 1;
    }
    keys.sort(compareNumeric);
    keys.map(function (key, indx) {
        apartmentsList.insertAdjacentElement('beforeend', SortElements[key]['element']);
    });
});

sortLevelUp.addEventListener('click', function (elem) {
    apartmentsArrowBtnButton.forEach((el) => {
        el.classList.remove('active__sort');
    });
    elem.currentTarget.classList.toggle('active__sort');
    apartmentsPrice.style.color = '#0b1739';
    apartmentsFloor.style.color = '#3EB57C';
    apartmentsSquares.style.color = '#0b1739';
    var items = document.querySelectorAll('.apartments__items');
    var SortElements = new Object();
    items.forEach(function (item, indx) {
        var itemValue = parseInt(
            item
                .querySelector('.apartments__items_level')
                .textContent.replace('руб', '')
                .replace(/\s+/g, ''),
        );
        SortElements[itemValue] = { element: item, index: indx };
    });
    var keys = Object.keys(SortElements);
    function compareNumeric(a, b) {
        a = parseInt(a);
        b = parseInt(b);
        if (a > b) return -1;
        if (a < b) return 1;
    }
    keys.sort(compareNumeric);
    keys.map(function (key, indx) {
        apartmentsList.insertAdjacentElement('beforeend', SortElements[key]['element']);
    });
});

sortLevelDown.addEventListener('click', function (elem) {
    apartmentsArrowBtnButton.forEach((el) => {
        el.classList.remove('active__sort');
    });
    elem.currentTarget.classList.toggle('active__sort');
    apartmentsPrice.style.color = '#0b1739';
    apartmentsFloor.style.color = '#3EB57C';
    apartmentsSquares.style.color = '#0b1739';
    var items = document.querySelectorAll('.apartments__items');
    var SortElements = new Object();
    items.forEach(function (item, indx) {
        var itemValue = parseInt(
            item
                .querySelector('.apartments__items_level')
                .textContent.replace('руб', '')
                .replace(/\s+/g, ''),
        );
        SortElements[itemValue] = { element: item, index: indx };
    });
    var keys = Object.keys(SortElements);
    function compareNumeric(a, b) {
        a = parseInt(a);
        b = parseInt(b);
        if (a < b) return -1;
        if (a > b) return 1;
    }
    keys.sort(compareNumeric);
    keys.map(function (key, indx) {
        apartmentsList.insertAdjacentElement('beforeend', SortElements[key]['element']);
    });
});

sortSquareUp.addEventListener('click', function (elem) {
    apartmentsArrowBtnButton.forEach((el) => {
        el.classList.remove('active__sort');
    });
    apartmentsSquares.style.color = '#3EB57C';
    apartmentsPrice.style.color = '#0b1739';
    apartmentsFloor.style.color = '#0b1739';
    elem.currentTarget.classList.toggle('active__sort');
    var items = document.querySelectorAll('.apartments__items');
    var SortElements = new Object();
    items.forEach(function (item, indx) {
        var itemValue = parseInt(
            item
                .querySelector('.apartments__items_squares')
                .textContent.replace('руб', '')
                .replace(/\s+/g, ''),
        );
        SortElements[itemValue] = { element: item, index: indx };
    });
    var keys = Object.keys(SortElements);
    function compareNumeric(a, b) {
        a = parseInt(a);
        b = parseInt(b);
        if (a > b) return -1;
        if (a < b) return 1;
    }
    keys.sort(compareNumeric);
    keys.map(function (key, indx) {
        apartmentsList.insertAdjacentElement('beforeend', SortElements[key]['element']);
    });
});

sortSquareDown.addEventListener('click', function (elem) {
    apartmentsArrowBtnButton.forEach((el) => {
        el.classList.remove('active__sort');
    });
    apartmentsSquares.style.color = '#3EB57C';
    apartmentsPrice.style.color = '#0b1739';
    apartmentsFloor.style.color = '#0b1739';
    elem.currentTarget.classList.toggle('active__sort');
    var items = document.querySelectorAll('.apartments__items');
    var SortElements = new Object();
    items.forEach(function (item, indx) {
        var itemValue = parseInt(
            item
                .querySelector('.apartments__items_squares')
                .textContent.replace('руб', '')
                .replace(/\s+/g, ''),
        );
        SortElements[itemValue] = { element: item, index: indx };
    });
    var keys = Object.keys(SortElements);
    function compareNumeric(a, b) {
        a = parseInt(a);
        b = parseInt(b);
        if (a < b) return -1;
        if (a > b) return 1;
    }
    keys.sort(compareNumeric);
    keys.map(function (key, indx) {
        apartmentsList.insertAdjacentElement('beforeend', SortElements[key]['element']);
    });
});

// Сортировка списка ------//

// Сброс фильтра //

filterReset.addEventListener('click', () => {
    defaultFilters();
});

function defaultFilters() {
    sliderPrice.noUiSlider.reset();
    sliderSquare.noUiSlider.reset();
    filterRoomInput.forEach((el) => {
        el.checked = false;
    });
    apartmentsArrowBtnButton.forEach((el) => {
        el.classList.remove('active__sort');
    });

    sortReset.forEach((el) => {
        el.style.color = '#0b1739';
    });
}

// Сброс фильтра -------//

//Кнопка вверх //

function upButton() {
    const body = document.querySelector('body'),
        div = document.createElement('div');

    div.className = 'up__btn';

    body.appendChild(div);

    div.addEventListener('click', () => {
        scrollTo(section);
    });

    window.addEventListener('scroll', function () {
        let heightPage = +this.pageYOffset;
        if (heightPage >= 350) {
            div.style.display = 'block';
        } else {
            div.style.display = 'none';
        }
    });
}

function scrollTo(elem) {
    window.scroll({
        top: elem.offsetTop,
        behavior: 'smooth',
    });
}

upButton();

//Кнопка вверх --------//
